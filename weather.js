const axios = require("axios");

const API_KEY = "81f8611edff955a7c802efa12bad2659";
const CITY_NAME = "Jakarta";
const UNITS = "metric";

URL = `https://api.openweathermap.org/data/2.5/forecast?q=${CITY_NAME},id&appid=${API_KEY}&units=${UNITS}`;

BASE_URL = "https://api.openweathermap.org/data/2.5/forecast";

module.exports = {
  forecastWeather: (cityName, units) =>
    axios({
      method: "GET",
      url: BASE_URL,
      params: {
        q: cityName,
        appid: API_KEY,
        units: units
      }
    })
};
// const forecastWeather = async (cityName, units) => {
//   await axios({
//     method: "GET",
//     url: BASE_URL,
//     params: {
//       q: cityName,
//       appid: API_KEY,
//       units: units
//     }
//   });
// };

// console.log(forecastWeather);
