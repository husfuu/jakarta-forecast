const weatherAPI = require("./weather");

const apiCall = async () => {
  const response = await weatherAPI.forecastWeather("Jakarta", "metric");
  console.log(response.data.data);
};

apiCall();
